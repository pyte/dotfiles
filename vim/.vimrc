" Pyte ViM light Configuration for Servers  
" pyte.dev

call plug#begin('~/.vim/plugged')

" Colorschemes
" --------------------------------------------------------------------------
" ayu theme
Plug 'ayu-theme/ayu-vim'

" Plugins
" --------------------------------------------------------------------------

" NerdTree Plugin for Filebrowser
Plug 'preservim/nerdtree'

" Lightline Plugin for Statusbar
Plug 'itchyny/lightline.vim'

" Devicons for vim (always load as last one)
Plug 'ryanoasis/vim-devicons'

" FZF
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

call plug#end()


" ---------------------------------------------------------------------------
" Global Configuration
" ---------------------------------------------------------------------------
let mapleader=" "
set ttyfast
set encoding=utf-8 nobomb

" History / File handling
set history=999
set undolevels=999
set autoread

" backup and swap handling
set nobackup
set nowritebackup
set noswapfile

" search handling
set hlsearch
set incsearch
set ignorecase smartcase

" split handling
set splitbelow splitright

" remove sounds and visual bell
set noerrorbells
set visualbell
set t_vb=

" Tab handling
set expandtab
set autoindent smartindent
set softtabstop=4
set tabstop=4
set shiftwidth=4
set smarttab

" Clipboard 
set clipboard=unnamed

" Tabs
map <leader>tn :tabnew<cr>
map <leader>t<leader> :tabnext<cr>
map <leader>tm :tabmove
map <leader>tc :tabclose<cr>
map <leader>to :tabonly<cr>

" User Interface 
"--------------------------------------------------------------------------
set t_Co=256
set t_ut=
set termguicolors

set background=dark
hi Normal guibg=NONE ctermbg=NONE
set number
set cursorline
set title

colorscheme ayu
" Ayu Theme Options
"let ayucolor="light"  " for light version of theme
"let ayucolor="mirage" " for mirage version of theme
let ayucolor="dark"   " for dark version of theme


" ---------------------------------------------------------------------------
" Keybindings Configuration
" ---------------------------------------------------------------------------
" Allow saving of files as sudo when I forgot to start vim using sudo.
cmap w!! w !sudo tee > /dev/null %

" Splits
nmap <C-h> <C-w>h
nmap <C-l> <C-w>l
nmap <C-k> <C-w><
nmap <C-j> <C-w>>

" Copy and Paste
vnoremap <Leader>y "**y
vnoremap <Leader>p "*p

" ---------------------------------------------------------------------------
" Plugin Configuration
" ---------------------------------------------------------------------------

" NerdTree Plugin Settings
map <Leader>ee :NERDTreeToggle<CR>
let NERDTreeShowHidden=1

" LightLine Plugin Settings
" Set these settings to remove delay and remove vims mode display
set ttimeoutlen=50
set laststatus=2
set noshowmode
let g:lightline = {
	\ 'colorscheme': 'ayu',
	\ }

" FZF Settings
nnoremap <C-f> :Files<cr>
