# Pytes Dotfiles

These dotfiles are manged within this git repository and deployed by [GNU Stow](https://www.gnu.org/software/stow/).
Clone the repository and symlink the configuration files with GNU Stow as you wish.

### How to use

To use this repository with GNU Stow you need to clone the repository and enable the packages as described below.

```bash
git clone https://gitlab.com/pyte/Dotfiles
cd Dotfiles
stow nvim
```

To update all dotfiles just update the local repository itself.

```bash
git pull
```

### Current Configs

The configs listed below are available in this repository. You may find additional information in the subfolders of each tool.

- [NeoVIM](https://gitlab.com/pyte/dotfiles/-/tree/main/nvim?ref_type=heads)
- [Bash](https://www.gnu.org/software/bash/)
- [Vim](https://www.vim.org)
