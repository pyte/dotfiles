# Pyte NeoVIM Configuration

- :rocket: Extremly fast due to [lazy.nvim](https://github.com/folke/lazy.nvim)
- :bulb: Easy to understand and customize
- :ledger: Extensively commented for simplicity

### Dependencies

This configuration has some external dependencies which need to been installed to make sure it works properly.

- [Nerd Font](https://nerdfonts.com) for gylph support
- [nodejs and npm](https://nodejs.org/en)
- [prettier](https://prettier.io/)
- [fzf](https://github.com/junegunn/fzf)
- [ripgrep](https://github.com/BurntSushi/ripgrep)
- [python3-pip](https://docs.python.org/3/installing/index.html)
- [python3-venv](https://docs.python.org/3/library/venv.html)
- [glow](https://github.com/charmbracelet/glow)

### Plugin manager

This configuration uses the [Lazy Plugin Manager](https://github.com/folke/lazy.nvim) for
it's sane and easy configuration and extremely fast loading times due to caching and bytecode compilation.

The configuration of the plugin manager itself can be found in `lazy.lua`.

### Structure

The configuration is structured as clean as possible. In the root folder e.g. `~/.config/nvim/` you'll
find the `init.lua` which just requires `pyte.core` and `pyte.lazy`. In the folder `lua/pyte` you can
find both. `pyte.core` references to the folder `lua/pyte/core` which contains it's own init file and the
core configurations e.g. keymaps and global options.
The file `lazy.lua` contains the plugin manager itself and imports the `pyte/plugins` and `pyte/plugins/lsp`
folders which contains the configuration for various plugins listed below.

### Core configuration

In the configuration file `keymaps.lua` you can find some of the keybind configurations that this config
uses among other things this includes: tab handling, colorscheme switching, searching. You can find more
information on the keybinds configured by the WhichKey plugin.

In the `options.lua` file are all of the global options configured. This includes among other things: tabs &
indents, searches, clipboard handling, splits.

### WhichKey Help

The plugin `WhichKey` helps with getting used to keybinds or even remembering keybinds that i only use occasionally.
When you hit the `<leader>` Key and just wait `WhichKey` will trigger and show all available options. If the keybinds
has multiple "levels" you can type `<leader>h` for example to see all keybinds that begin with `<leader>` and `h`.

Keybinds are added automatically to the WhichKey plugin if the necessary fields are set e.g. `desc` in the keybind 
definition. An example:

```lua
vim.keymap.set("n", "<leader>ha", function() harpoon:list():append() end, { desc = "Append file to harpoon list" })
```

### Plugins

Plugins are configured in the `plugins` and `plugins/lsp` folder. Usually each plugin has its own file named
after the plugin itself. However some plugins are only there as a dependencies of others.

Below are listed the plugins which are currently in use by this configuration:

- [autopairs](https://github.com/windwp/nvim-autopairs)
- [bufferline](https://github.com/akinsho/bufferline.nvim)
- [colorizer](https://github.com/NvChad/nvim-colorizer.lua)
- [catppuccin](https://github.com/catppuccin/nvim)
- [moonfly](https://github.com/bluz71/vim-moonfly-colors)
- [dressing](https://github.com/stevearc/dressing.nvim)
- [formatter](https://github.com/stevearc/conform.nvim)
- [gitsigns](https://github.com/lewis6991/gitsigns.nvim)
- [ident-blankline](https://github.com/lukas-reineke/indent-blankline.nvim)
- [lualine](https://github.com/nvim-lualine/lualine.nvim)
- [nvim-cmp](https://github.com/hrsh7th/nvim-cmp)
- [nvim-tree](https://github.com/nvim-tree/nvim-tree.lua)
- [nvim-treesitter](https://github.com/nvim-treesitter/nvim-treesitter)
- [devicons](https://github.com/nvim-tree/nvim-web-devicons)
- [telescope](https://github.com/nvim-telescope/telescope.nvim)
- [whichkey](https://github.com/folke/which-key.nvim)
- [lspconfig](https://github.com/neovim/nvim-lspconfig)
- [mason](https://github.com/williamboman/mason.nvim)
- [none-ls](https://github.com/nvimtools/none-ls.nvim)
- [harpoon](https://github.com/ThePrimeagen/harpoon/tree/harpoon2)
- [nvim-devdocs](https://github.com/luckasRanarison/nvim-devdocs)
