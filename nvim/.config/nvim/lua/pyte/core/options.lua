local opt = vim.opt -- save letters

-- line numbers
opt.relativenumber = true -- relative line numbers
opt.number = true -- enable line numbers

-- tabs & indent
opt.tabstop = 2 -- 2 spaces for a tab
opt.shiftwidth = 2 -- 2 spaces for indents
opt.expandtab = true -- tabs to spaces
opt.autoindent = true -- if current line is indent next one is too

-- line wraps
opt.wrap = false -- don't wrap lines, i like em long

-- search 
opt.ignorecase = true -- ignore case while searing
opt.smartcase = true -- When upper assume we want case sensetive

-- cursor line
opt.cursorline = true -- highlight the current line for focus

-- appearance settings

-- enable termguicolors as most colorscheme need this
-- terminal emulator needs to support this
opt.termguicolors = true
opt.background = "dark"
opt.signcolumn = "yes"

-- backspace 
opt.backspace = "indent,eol,start" -- allow backspace on indent, end of line or insert mode start pos

-- clipboard
opt.clipboard:append("unnamedplus") -- use system clipboard by default

-- splits
opt.splitright = true -- split vertical to the right
opt.splitbelow = true -- split horizontal to the bottom

-- turn off swapfile
opt.swapfile = false


