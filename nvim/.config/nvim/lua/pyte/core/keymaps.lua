--leader is space
vim.g.mapleader = " "

local keymap = vim.keymap -- less to type

-- Keymaps

-- clear search highlights
keymap.set("n", "<leader>nh", ":nohl<CR>", { desc = "Clear search highlights" })

-- remap x so that it copys into void and not clipboard
keymap.set("n", "x", '"_x')

-- increment/decrement numbers
keymap.set("n", "<leader>+", "<C-a>", { desc = "Increment number" }) -- increment
keymap.set("n", "<leader>-", "<C-x>", { desc = "Decrement number" }) -- decrement

-- buffer handling
keymap.set("n", "<leader>bn", "<cmd>bnext<CR>", { desc = "Next buffer" })
keymap.set("n", "<leader>bp", "<cmd>bprev<CR>", { desc = "Previous buffer" })
keymap.set("n", "<leader>bc", "<cmd>bd<CR>", { desc = "Close buffer" })
keymap.set("n", "<leader>bo", "<cmd>enew<CR>", { desc = "New buffer" })
keymap.set("n", "<leader>bl", "<cmd>ls<CR>", { desc = "List buffers" })
keymap.set("n", "<C-n>", "<cmd>bnext<CR>", { desc = "Next buffer" })
keymap.set("n", "<C-m>", "<cmd>bprev<CR>", { desc = "PRevious buffer" })



-- split movement
keymap.set("n", "<C-h>", "<C-w>h", { desc = "Go to split on the left" })
keymap.set("n", "<C-l>", "<C-w>l", { desc = "Got to split on the right" })
keymap.set("n", "<C-j>", "<C-w>j", { desc = "Go to split on the bottom" })
keymap.set("n", "<C-k>", "<C-w>k", { desc = "Go to split on the top" })

-- colorscheme switcher
keymap.set(
  "n",
  "<leader>csl",
  "<cmd>colorscheme catppuccin-latte<CR>",
  { desc = "Switch to Catppuccin Latte colorscheme" }
) -- switch to light theme
keymap.set(
  "n",
  "<leader>csd",
  "<cmd>colorscheme catppuccin-frappe<CR>",
  { desc = "Switch to Catppuccin Frappe colorscheme" }
) -- switch to dark theme
