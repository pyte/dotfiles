return {
  "luckasRanarison/nvim-devdocs",
  dependencies = {
    "nvim-lua/plenary.nvim",
    "nvim-telescope/telescope.nvim",
    "nvim-treesitter/nvim-treesitter",
  },
  opts = {},
  config = function()
    local nvimdevdocs = require("nvim-devdocs")
    local keymap = vim.keymap -- for conciseness

    keymap.set("n", "<leader>sd", ":DevdocsToggle<CR>", { desc = "Toggle Devdocs" })
    keymap.set("n", "<leader>ss", ":DevdocsOpenFloat<CR>", { desc = "Search Devdocs" })



    nvimdevdocs.setup({
      dir_path = vim.fn.stdpath("data") .. "/devdocs",
      telescope = {},
      float_win = {
        relative = "editor",
        height = 25,
        width = 100,
        border = "rounded",
      },
      wrap = false,                                   -- text wrap, only applies to floating window
      previewer_cmd = "glow",                         -- for example: "glow"
      cmd_args = { "-s", "dark", "-w", "80" },        -- example using glow: { "-s", "dark", "-w", "80" }
      cmd_ignore = {},                                -- ignore cmd rendering for the listed docs
      picker_cmd = true,                              -- use cmd previewer in picker preview
      picker_cmd_args = { "-s", "dark", "-w", "40" }, -- example using glow: { "-s", "dark", "-w", "50" }
      mappings = {                                    -- keymaps for the doc buffer
        open_in_browser = ""
      },
      ensure_installed = {}, -- get automatically installed
      after_open = function(bufnr)
        vim.api.nvim_buf_set_keymap(bufnr, 'n', '<ESC>', ':close<CR>', {})
      end
    })
  end,
}
