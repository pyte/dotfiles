return {
  "ThePrimeagen/harpoon",
  branch = "harpoon2",
  dependencies = { "nvim-lua/plenary.nvim" },
  config = function()
    local harpoon = require("harpoon")

    harpoon:setup()
    local keymap = vim.keymap -- for conciseness

    keymap.set("n", "<leader>ha", function() harpoon:list():append() end, { desc = "Append file to harpoon list" })
    keymap.set("n", "<leader>he", function() harpoon.ui:toggle_quick_menu(harpoon:list()) end,
      { desc = "Toggel harpoon UI" })
    keymap.set("n", "<leader>hp", function() harpoon:list():prev() end, { desc = "Select next harpooned buffer" })
    keymap.set("n", "<leader>hn", function() harpoon:list():next() end, { desc = "Select previous harpooned buffer" })
  end,

}
