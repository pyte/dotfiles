return {
  {
    --"olimorris/onedarkpro.nvim",
    "tiagovla/tokyodark.nvim",
    lazy = false,
    priority = 1000,                     -- this should load before anything else
    config = function()
      vim.cmd([[colorscheme tokyodark]]) -- tokyonight-{night,storm,day,moon}
    end,
  },
}
